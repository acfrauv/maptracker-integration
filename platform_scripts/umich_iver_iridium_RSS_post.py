
from time import sleep
from platform_tools.platformdata import APIPlatform, get_args
import pprint
import feedparser
from parse import parse, search

platform_key = 'DROPIVER.IRIDIUM'
RSS_URL = 'https://zapier.com/engine/rss/2895871/dropiveriridium/'
DELAY = 60

class SlocumGliderPlatform(APIPlatform):
    def start_threads(self, platform_key):
        last_heard_time = None
        while True:
            try:
                data, message_time = self.get_last_rss(RSS_URL)

                if (message_time != last_heard_time):
                    mission_data = {
                        'pose': {
                            'lat': float(data["lat"]),
                            'lon': float(data["lon"]),
                            'uncertainty': float(11),
                            'speed': float(data["speed"])*0.514444,   # convert knots to m/s
                            'heading': float(data["heading"])
                        },
                        'stat': {
                            # "sent-UTC": data["timestamp"],
                            "received": message_time,
                            'bat': int(float(data["bat"])),
                            'mode': data['mode'],
                            'wpt#': data["wpt"],
                            # 'sent': message_time,
                            # "error": data["Error"]
                        },
                        'alert': {}
                    }
                    # pprint.pprint(mission_data)
                    last_heard_time = message_time
                    #pprint.pprint(mission_data)
                    self.update_platform_data(platform_key, mission_data)
            except Exception as e:
                err_str = 'Issue with fetching RSS data: %s' %repr(e)
                print(err_str)
                self.log_platform_message(platform_key, 'warning', err_str)
            sleep(DELAY)

    def get_last_rss(self, url):
        d = feedparser.parse(url)
        message_body = d.entries[0].description
        message_time = d.entries[0].published

        #message_format= 'MOMSN: {momsn}\n MTMSN: {mtmsn}\n Time of Session (UTC): {timestamp}\n Session Status: {status}\n Message Size (bytes): {bytes:d}\n\n Unit Location: Lat = {lat:f} Long = {lon:f}\n CEPradius = {eprad:d}'

        #features_dict = parse(message_format, message_body)

        #IVER3-3047, lat=20.8403, long=-156.6805, spd=0.6, hdg=2, Wp=32, bat=78.9, act=1, err=N, mode=Parking, imie=300234063485500 See in google maps (http://maps.google.com/maps?q=20.8403,+-156.6805+(IVER3-3047 H=2 S=0.6 act=1 B=78.9 Err=N )&iwloc=A&hl=en)

        features_dict = {
            "lat": search("lat={}, ", message_body)[0],
            "lon": search("long={},", message_body)[0],
            "speed": search("spd={},", message_body)[0],
            "heading": search("hdg={},", message_body)[0],
            "bat": search("bat={},", message_body)[0],
            "mode": search("mode={},", message_body)[0],
            "wpt": search("Wp={},", message_body)[0]
        }

        # print (message_body)
        # pprint.pprint(features_dict)

        return features_dict, message_time




if __name__ == "__main__":

    # get cli args
    args = get_args()

    # Instantiate class
    platform = SlocumGliderPlatform(maptracker_url=args["url"], maptracker_port=args["port"])

    # Start data threads
    platform.start_threads(platform_key)

    # keep main thread alive
    platform.serve_forever()
