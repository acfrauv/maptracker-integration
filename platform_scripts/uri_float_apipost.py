######################################################################
# This file handles all the platform pose updates.
# 
# An LCM thread listens for incoming messages and they are forwarded
# to Maptracker as HTTP POST messages.
# 
#
######################################################################

import time
import threading
import sys
import math
import select
import os
import collections
import requests
import lcm
from ctypes import c_uint8
import StringIO
try:
    import acfr_missionsDS
    import pyproj
    acfr_missionsDS_supported = True
except:
    print 'Could not find acfr_missionsDS python library.  Sending commands to lcm vehicles will not be supported'
    acfr_missionsDS_supported = False

LCMROOT ='/home/auv/git/acfr-lcm'
sys.path.append('{0}/build/lib/python{1}.{2}/dist-packages/perls/lcmtypes/'.format(LCMROOT, sys.version_info[0], sys.version_info[1]))


from senlcm import usbl_fix_t, uvc_osi_t
from urioce_lcm import float_acomm_status_t
from acfrlcm.auv_global_planner_t import auv_global_planner_t
from platform_tools.platformdata import APIPlatform, get_args, get_validated


# --------------------------------------LCM-THREAD------------------------------------- #

class LcmThread(threading.Thread, APIPlatform):

    def __init__(self, **kwargs):
        threading.Thread.__init__(self)
        self.lc = lcm.LCM()
        self.exitFlag = False
        self.daemon = True  # run in daemon mode to allow for ctrl+C exit
        self.pfloatMsg = None
        self.lat = None
        self.lon = None
        APIPlatform.__init__(self, **kwargs)

    def usblFixHandler(self, channel, data):
        print 'Received usbl_fix on channel ' + channel
        msg = usbl_fix_t.decode(data)

        self.lat = round(msg.latitude*180/math.pi, 8)                                             # float, decimal degrees
        self.lon = round(msg.longitude*180/math.pi, 8)                                             # float, decimal degrees
        self.postUpdate()

        # the rest of this function should be used if you are handling and dsiplaying USBL fixes
        # on Falkor the acfr handler is handling this
        platform_data = {
            # All alert key-value pairs are optional. For no alerts, include empty dict, ie {}
            # The keys can be anything, but keep them short for display.
            # The values need to be int 0/1, with 1=on, 0=off.
            "alert": {
            },
            
            # required: lat, lon
            # optional, but required for dash display: heading, roll, pitch, bat
            # optional, speed, uncertainty, alt, depth
            # all values must be floats
            "pose": {
                "lat": round(msg.latitude*180/math.pi, 8),                                             # float, decimal degrees
                "lon": round(msg.longitude*180/math.pi, 8),                                             # float, decimal degrees
                "depth": round(msg.depth, 1),                           # float, m
                "uncertainty": round(msg.accuracy, 2)                  # float, m
            },

            # All stat key-value pairs are optional. For no stats, include empty dict, ie {}
            # optional, but required for dash display: bat
            # The keys can be anything, but keep them short for display.
            # The values can be anything, with the exception of bat, which must be an int
            "stat": {
                'XYZ': "{}, {}, {}".format(round(msg.target_x, 1), round(msg.target_y, 1), round(msg.target_z, 1))
            }
        }

        # Use the LCM Channel name as the platform_key so can tell which vehicle the status came from
        # This should match the url field in the config json file for the vehicle
        platform_key = str(channel)

        # And POST the data 
        #self.update_platform_data(platform_key, platform_data)

    def shipStatusHandler(self, channel, data):
        # this function can be used to pass ship status data to maptracker
        msg = ship_status_t.decode(data)

        lat = round(msg.latitude*180/math.pi, 8)
        lon = round(msg.longitude*180/math.pi, 8)

        platform_data = {
            # All alert key-value pairs are optional. For no alerts, include empty dict, ie {}
            # The keys can be anything, but keep them short for display.
            # The values need to be int 0/1, with 1=on, 0=off.
            "alert": {
            },

            # required: lat, lon
            # optional, but required for dash display: heading, roll, pitch, bat
            # optional, speed, uncertainty, alt, depth
            # all values must be floats
            "pose": {
                "lat": lat,    						# float, decimal degrees
                "lon": lon,     					# float, decimal degrees
                "heading": round(math.degrees(msg.heading), 2),  	# float, degrees, range: 0:360
                "pitch": round(math.degrees(msg.pitch), 2),      	# float, degrees, range: -180:180
                "roll": round(math.degrees(msg.roll), 2)         	# float, degrees, range: -180:180
            },

            # All stat key-value pairs are optional. For no alerts, include empty dict, ie {}
            # optional, but required for dash display: bat
            # The keys can be anything, but keep them short for display.
            # The values can be anything, with the exception of bat, which must be an int
            "stat": {
            }

        }

        # Use the LCM Channel name as the platform_key so can tell which vehicle the status came from
        # This should match the url field in the config json file for the vehicle
        platform_key = str(channel)

        # And POST the data 
        self.update_platform_data(platform_key, platform_data)

    def pfloatHandler(self, channel, data):
        self.pfloatMsg = float_acomm_status_t.decode(data)                               
        print 'Received float_acomm_status_t on channel ' + channel 

    def postUpdate(self):
        if self.lat is not None and self.lon is not None and self.pfloatMsg is not None:
            platform_data = {
                # All alert key-value pairs are optional. For no alerts, include empty dict, ie {}
                # The keys can be anything, but keep them short for display.
                # The values need to be int 0/1, with 1=on, 0=off.
                "alert": {
                    "arduino":  bool(self.pfloatMsg.arduino_status),
                    "ctl":  bool(self.pfloatMsg.ctl_status),
                    "mis":  bool(self.pfloatMsg.mis_status),
                },
    
                # required: lat, lon
                # optional, but required for dash display: heading, roll, pitch, bat
                # optional, speed, uncertainty, alt, depth
                # all values must be floats
                "pose": {
                    "lat": float(self.lat),     					# float, decimal degrees
                    "lon": float(self.lon),     					# float, decimal degrees
                    "alt": self.pfloatMsg.altitude/100.0,                   # float, m
                    "depth": self.pfloatMsg.depth/100.0,                       	# float, m [sent as 10*m]
                    "speed": 0.5  # hard code this to get the increasing circle of uncertainty. This could possibly come from VO
                },
    
                # All stat key-value pairs are optional. For no alerts, include empty dict, ie {}
                # optional, but required for dash display: bat
                # The keys can be anything, but keep them short for display.
                # The values can be anything, with the exception of bat, which must be an int
                "stat": {
                    "bat": self.pfloatMsg.bat_charge,                               # int, %, range: 0:100
                    "ctl mode": self.pfloatMsg.ctl_mode,
                    "vol cmd": str(self.pfloatMsg.volume_cmd) + ' (mL)',
                    "vol act": str(self.pfloatMsg.volume_actual) + ' (mL)',
                    "thr cmd": str(float(self.pfloatMsg.thrust_cmd)/50.0) + ' (kg)',
                    "leg num": self.pfloatMsg.leg_num,
                    "leg time": str(self.pfloatMsg.leg_time) + ' (s)',
                    "leg type": self.pfloatMsg.leg_type,
                    "mis time": str(self.pfloatMsg.mis_time) + ' (s)'
                    #"#imgs": self.pfloatMsg.img_count # this would be a good thing to report
                }
            }

            # Use the LCM Channel name as the platform_key so can tell which vehicle the status came from
            # This should match the url field in the config json file for the vehicle
            platform_key = "PFLOAT.AUVSTAT"

            # And POST the data 
            self.update_platform_data(platform_key, platform_data)

    def run(self):
        #self.lc.subscribe(".*\.AUVSTAT", self.auvStatusHandler)
        #self.lc.subscribe(".*\.SHIP_STATUS", self.shipStatusHandler)
        #self.lc.subscribe(".*\.USBL_FIX\..*", self.usblFixHandler)
        self.lc.subscribe("FALKOR.USBL_FIX.PFLOAT", self.usblFixHandler)
        self.lc.subscribe("AFS.PFLOAT", self.pfloatHandler)

        timeout = 1  # amount of time to wait, in seconds for subscriptions

        # Main loop just handling incoming LCM messages (unless self destruct)
        while not self.exitFlag:
            rfds, wfds, efds = select.select([self.lc.fileno()], [], [], timeout)
            if rfds:
                self.lc.handle()

    def execute_command(self, platform, args):
        
        cmd = get_validated(args, 'command', val_type=basestring, val_inlist=["goto", "abort", "text"])
        # recover the remaining variables depending on the command
        if cmd == "goto":
            lon = get_validated(args, 'lon', val_convert=float, val_type=float, val_min=-180, val_max=180, required=True)  # float(args['lon'])
            lat = get_validated(args, 'lat', val_convert=float, val_type=float, val_min=-90, val_max=90, required=True)  # float(args['lat'])
            depth = get_validated(args, 'depth', val_convert=float, val_type=float, required=True)  # float(args['depth'])
            velocity = get_validated(args, 'velocity', val_convert=float, val_type=float, val_min=0, required=True)  # float(args['velocity'])
            timeout = get_validated(args, 'timeout', val_convert=float, val_type=float, val_min=0, required=True)  #float(args['timeout'])
        elif cmd == "state_change":
            new_state = args['new_state']
        elif cmd == "text":
            text_body = args['text_body'] 
  
        if platform == "SIRIUS":
            if cmd == "goto":
                msg = auv_global_planner_t()
                msg.point2_x = lon
                msg.point2_y = lat
                msg.point2_z = depth
                msg.point2_att[2] = float(-98765)
                msg.velocity[0] = velocity
                msg.timeout = timeout
                msg.command = auv_global_planner_t.GOTO
            elif cmd == "abort":
                msg = auv_global_planner_t()
                msg.command = auv_global_planner_t.ABORT
                msg.str = "MANUAL"
                # lc.publish('TASK_PLANNER_COMMAND.{}'.format(platform), msg.encode())
            self.lc.publish('{}.TASK_PLANNER_COMMAND'.format(platform), msg.encode())
            return "The '{}.TASK_PLANNER_COMMAND' LCM message has been published".format(platform)
        else:
            if (cmd == "goto") and (acfr_missionsDS_supported == True):
                print 'Setting up globals'
                # set up globals - some of these should come from the param server
                olat = 20.819721
                olon = -156.681945
                origin = acfr_missionsDS.latlonelement_t(olat, olon)
                turn_radius = acfr_missionsDS.distanceelement_t(0.4)
                drop_distance = acfr_missionsDS.distanceelement_t(1)
                drop_angle = acfr_missionsDS.angleelement_t(30)
                mission_timeout = acfr_missionsDS.timeelement_t(1000)
                glob = acfr_missionsDS.globals_t(origin, turn_radius, drop_distance, drop_angle, mission_timeout)

                projStr = '+proj=tmerc +lon_0={} +lat_0={} +units=m'.format(olon, olat)
                p = pyproj.Proj(projStr)

                print 'Setting up location'
                y, x = p(lon, lat)
                pos = acfr_missionsDS.localpositionelement_t(x, y, depth)
                hdg = acfr_missionsDS.angleelement_t(90)
                timeout = acfr_missionsDS.timeelement_t(timeout)
                vel = acfr_missionsDS.velocityelement_t(velocity)
                depthMode = acfr_missionsDS.depthelement_t('depth')

                print 'Setting up goto'
                goto_prim = acfr_missionsDS.goto_t(pos, hdg, timeout, vel, depthMode)
                prim = acfr_missionsDS.commandprimitive_t()
                prim.set_goto(goto_prim)

                print 'Setting up mission'
                mis = acfr_missionsDS.mission_t(glob)
                mis.add_primitive(prim)

                output = StringIO.StringIO()
                mis.export(output, 0, '', 'mission')
                print 'Sending mission'
                print output.getvalue()

                # now send the actual mission
                msg = auv_global_planner_t()
                msg.command = auv_global_planner_t.GOTO
                msg.str = output.getvalue()
                
                self.lc.publish('{}.TASK_PLANNER_COMMAND'.format(platform), msg.encode())
                return "The 'TASK_PLANNER_COMMAND.{}' LCM message has been published".format(platform)

            elif cmd == "abort":
                msg = auv_global_planner_t()
                msg.command = auv_global_planner_t.ABORT
                msg.str = "ABORT"
                self.lc.publish('{}.TASK_PLANNER_COMMAND'.format(platform), msg.encode())
                return "The '{}.TASK_PLANNER_COMMAND' LCM message has been published".format(platform)
             
            elif cmd == "state_change":
                msg = auv_global_planner_t()
                if new_state == "abort_reset": msg.command = auv_global_planner_t.RESET
                elif new_state == "pause": msg.command = auv_global_planner_t.PAUSE
                elif new_state == "resume": msg.command = auv_global_planner_t.RESUME
                elif new_state == "skip": msg.command = auv_global_planner_t.SKIP
                elif new_state == "stop": msg.command = auv_global_planner_t.STOP
                msg.str = "STATE_CHANGE"
                self.lc.publish('{}.TASK_PLANNER_COMMAND'.format(platform), msg.encode())
                return "The '{}.TASK_PLANNER_COMMAND' LCM message has been published".format(platform)
 
            elif cmd == "text":
                msg = auv_global_planner_t()
                msg.command = auv_global_planner_t.MISSION
                msg.str = text_body
                self.lc.publish('{}.TASK_PLANNER_COMMAND'.format(platform), msg.encode())
                return "The '{}.TASK_PLANNER_COMMAND' LCM message has been published".format(platform)
 
            else:
                print "*** NOT IMPLEMENTED:", platform, args
                raise NotImplementedError("Command interpreter not implemented for: "+platform)


# ----------------------------------END-LCM-THREAD------------------------------------- #


# --------------------------------------MAIN------------------------------------------- #

def main():
    # get optional args
    args = get_args()

    print 'Instantiating lcm object'
    # instantiate object
    acfrplatforms = LcmThread(maptracker_url=args["url"], maptracker_port=args["port"], command_server_port=args["command_port"])

    print 'Starting threads'
    # start threads
    acfrplatforms.start()

    print 'Waiting for messages'
    # keep alive
    acfrplatforms.serve_forever()

# ----------------------------------END-MAIN------------------------------------------- #

if __name__ == "__main__":
    main()


