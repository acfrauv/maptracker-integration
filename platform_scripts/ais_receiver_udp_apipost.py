# !/usr/bin/env python

import ais
import argparse
import threading
import time
import socket
from geojson import Point, FeatureCollection, LineString, Feature
import LatLon
from platform_tools.platformdata import post_data


# MMSIs
# SOI Falkor: ais_receiver_udp_apipost.py -m 319005600 -t 983191003=ATREU.AIS


parser = argparse.ArgumentParser(description="Maptracker AIS forwarder")

# Generator options
parser.add_argument("-a", "--ais_port", help="UDP port to listen to for AIS data (eg: 10000)", default=10000, dest="ais_port", type=int)
parser.add_argument("-u", "--maptracker_url", help="HTTP address for Maptracker API (eg: http://127.0.0.1)", default="http://127.0.0.1", dest="maptracker_url")
parser.add_argument("-p", "--maptracker_port", help="HTTP port for Maptracker API (eg: 8080)", default=8080, dest="maptracker_port", type=int)
parser.add_argument("-d", "--post_delay", help="Delay between updates to Maptracker API in seconds (eg: 10)", default=10, dest="post_delay", type=float)
parser.add_argument("-m", "--my_mmsi", help="The MMSI of this ship/receiver (plots differently)", default=None, dest="my_mmsi", type=int)
parser.add_argument("-v", "--vector_duration", help="The duration to plot the vector length in s (eg: 360=6min)", default=360, dest="vector_duration", type=float)
parser.add_argument("-t", "--target", help="Post as maptracker target, not AIS layer (eg: -t 319005600=FALKOR.AIS )",  action='append', type=lambda kv: kv.split("="), dest='targets')

# LOG_FORMAT = "%(asctime)s %(levelname)s [%(module)s:%(lineno)d]: %(message)s"
MAX_SHIP_AGE = 20*60  # 20 minutes
THIS_SHIP_MMSI = 319005600

INTERESTING_FIELDS = [
    "true_heading",
    "x",
    "y",
    "sog",
    "cog",
    "name",
    "callsign",
    "destination",
    "draught",
    "mmsi"
]


lock = threading.RLock()
ships = {}


def get_ship_info(decoded):
    global ships
    ship_id = decoded["mmsi"]
    if ship_id in ships:
        ship = ships[ship_id]
        print("*** UPDATING SHIP! {} ({})".format(ship["name"], ship_id))
    else:
        ship = {"name": "UNKNOWN"}
        print("*** NEW SHIP! {} ({})".format(ship["name"], ship_id))

    with lock:
        ship["last_seen"] = time.time()
        for field in INTERESTING_FIELDS:
            if field in decoded:
                val = decoded[field].rstrip("@") if isinstance(decoded[field], basestring) else decoded[field]
                ship[field] = val
        ships[ship_id] = ship

    return ship, ship_id


def remove_ship(mmsi):
    """Remove a ship from the register."""
    global ships
    with lock:
        del ships[mmsi]


def process_ais_messages(q):
    """Read parsed AIS messages from q and update the register."""
    while True:
        msg = q.get()
        try:
            if "decoded" in msg and "mmsi" in msg["decoded"]:
                ship, ship_id = get_ship_info(msg["decoded"])
        except Exception as e:
            print ("*** ERROR: {}".format(e))


def listen_for_messages(s, q):
    """Read raw AIS messages from the socket s and parse them onto the queue q."""
    while True:
        data, addr = s.recvfrom(1024)
        # log.info("Received message", data)
        try:
            q.put(data.decode())
        except Exception as e:
            print ("*** ERROR: decoding {}".format(e))
            # log.exception("Caught error decoding: %s", e)


def forward_changes(maptracker_url, maptracker_port, post_delay, my_mmsi=None, vector_duration=360, targets={}):
    """Forward changes to the register to Maptracker API."""
    while True:
        try:
            feature_list = []
            for ship_key in ships.keys():
                ship = ships[ship_key]
                if ship["last_seen"] < time.time() - MAX_SHIP_AGE:
                    remove_ship(ship_key)
                else:
                    if str(ship_key) in targets.keys():
                        data = {"pose": {"lat": round(ship["y"],8), "lon": round(ship["x"],8), "heading": round(ship["cog"],1), "speed": round(ship["sog"],1)}, "alert": {}, "stat":{}}
                        post_data(targets[str(ship_key)], data, data_type="platform", maptracker_port=maptracker_port, maptracker_url=maptracker_url)
                    else:
                        if "x" in ship and "y" in ship:
                            point = Point([ship["x"], ship["y"]])
                            color = "white" if str(ship_key) == str(my_mmsi) else "red"
                            radius = 3 if str(ship_key) == str(my_mmsi) else 15

                            # Add ship marker
                            feature_list.append(Feature(geometry=point, properties={
                                # Styling options
                                "options": {"radius": radius, "fillColor": "black", "color": color, "weight": 3,
                                            "opacity": 0.8, "fillOpacity": 0.5, "dashArray": "2,2"},
                                "info": ship
                            }))

                            # Add ship vector
                            if "sog" in ship and "cog" in ship:
                                ll = LatLon.LatLon(ship["y"], ship["x"])
                                vector_lenth = float(ship["sog"])*0.514444*vector_duration   # convert sog in length in m for 6 min
                                new_ll = ll.offset(float(ship["cog"]), vector_lenth/1000)
                                feature_list.append(Feature(geometry=LineString([(ship["x"], ship["y"]), (float(new_ll.lon), float(new_ll.lat))]), properties={
                                    "options": {"color": color, "weight": 3, "opacity": 0.8},
                                    "info": ship
                                }))

            # Post data to maptracker
            geojson_data = FeatureCollection(feature_list)
            post_data("AIS_LAYER", geojson_data, data_type="mission", maptracker_port=maptracker_port, maptracker_url=maptracker_url)

        except Exception as e:
            # log.exception(e)
            print ("*** ERROR: {}".format(e))

        time.sleep(post_delay)



def start_thread(target, args=(), kwargs={}):
    t = threading.Thread(target=target, args=args, kwargs=kwargs)
    t.daemon = True
    t.start()
    return t


def main():
    options = parser.parse_args()

    print ("Waiting for AIS messages on port %d, forwarding to Maptracker at %s" % (options.ais_port, options.maptracker_url))

    receive_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    receive_sock.bind(("", options.ais_port))

    parse_queue = ais.vdm.BareQueue()

    listener = start_thread(target=listen_for_messages, args=(receive_sock, parse_queue))

    forwarder = start_thread(
        target=forward_changes,
        args=(options.maptracker_url, options.maptracker_port, options.post_delay,),
        kwargs=dict(my_mmsi=options.my_mmsi, targets=dict(options.targets), vector_duration=options.vector_duration)
    )

    process_ais_messages(parse_queue)


if __name__ == "__main__":
    main()
