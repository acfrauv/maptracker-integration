# -*-* coding: utf-8 -*-*
from __future__ import print_function
from time import sleep, time
from platform_tools.platformdata import APIPlatform, get_args
import thread
import json
from random import randrange
import requests
import sys


# Platform key: must be unique
platform_key = "iridium"

SERVER_URL = 'http://www-personal.acfr.usyd.edu.au/groussel'
PAGE_URL = '{}/iridium/iridium.py'.format(SERVER_URL)

# mails are sent every 5 minutes
UPDATE_PERIOD = 30


cgi_data = """
{
    "folder": "SBD Sirius Iridium Beacon 300434061132890",
    "data": {
        "gps_time": 1210757133,
        "hdop": 3,
        "msg_type": "gps",
        "hexdata": "01fd00482ab00da603392fdf12db12ea050f2b",
        "sbd_filename": "300434061132890_001424.sbd",
        "lat": -35.920545000000004,
        "lon": 136.34711399999998,
        "valid": true,
        "error": null,
        "sats_visible": 7,
        "utc_time": "2018-05-19 09:25:16"
    },
    "message_id": 2847,
    "processed_time": 1529652057,
    "subject": "SBD Msg From Unit: 300434061132890"
}
"""


class IridiumPlatform(APIPlatform):
    def __init__(self, **kwargs):
        # self.origin = [-33.8405831822, 151.2548891302]  # not needed for a real vehicle, this is just for fake data
        # default origin AU-AU channel, hawaii
        self.origin = [20.72404658, -156.66]
        APIPlatform.__init__(self, **kwargs)

    def platformdata_update_thread(self, platform_key):
        """
        NOTE: This should be a thread that subscribes to a UDP feed or similar to receive nav updates. It should
        construct the nav data object as done here, but when new data is received.
        """
        while True:
            iri_message = self.get_iridium_data()
            if iri_message is not None:
                apipost = self.build_apipost(iri_message)
                self.update_platform_data(platform_key, apipost)
            # mails are received every 5 min
            sleep(5)

    def build_apipost(self, iri_message):
        """
        Builds a valid apipost message from a Iridium-based dict
        
        Args:
            iri_message (dict): iridium message
        
        Returns:
            dict: maptracker api message
        """
        iri_data = iri_message['data']
        data = {
            # All alert key-value pairs are optional. For no alerts, include empty dict, ie {}
            # The keys can be anything, but keep them short for display.
            # The values need to be int 0/1, with 1=on, 0=off.
            "alert": {
                'valid': int(iri_data['valid'])
            },

            # required: lat, lon, heading
            # optional, but required for dash display: roll, pitch, bat
            # optional, speed, uncertainty, alt, depth
            # all values must be floats
            "pose": {
                # float, decimal degrees
                "lat": iri_data['lat'],
                # float, decimal degrees
                "lon": iri_data['lon']
            },

            # All stat key-value pairs are optional. For no alerts, include empty dict, ie {}
            # optional, but required for dash display: bat
            # The keys can be anything, but keep them short for display.
            # The values can be anything, with the exception of bat, which must be an int
            "stat": {
                'valid': iri_data['valid'],
                "error": iri_data['error'],
                "sats_visible": iri_data['sats_visible'],
                "message_id": iri_message['message_id'],
                "timestamp": iri_data['utc_time']
            }
        }
        return data


    def get_iridium_data_fake(self):
        """
        Fake message generator
        
        Returns:
            dict: an iridium message
        """
        iri_message = json.loads(cgi_data)
        return iri_message

    def get_iridium_data(self):
        """
        Message retriever.

        GET the server for a parsed iridium JSON data (CGI-BIN script)
        
        Returns:
            dict: iridium message
        """
        try:
            r = requests.get(PAGE_URL)
            if r.ok:
                iri_message = r.json()
                print('Iridium: http request SUCCESS')
                return iri_message
            else:
                raise requests.ConnectionError('Request fail ( HTTP {})'.format(r.status_code))
        except requests.ConnectionError as err:
            print('Iridium: HTTP request FAILED: {}'.format(err))
            return None
    
    def start_threads(self, platform_key):
        thread.start_new_thread(
            platform.platformdata_update_thread, (platform_key,))


if __name__ == "__main__":

    # get cli args
    args = get_args()

    # Instantiate class
    platform = IridiumPlatform(
        maptracker_url=args["url"], maptracker_port=args["port"], command_server_port=args["command_port"])

    # Start data threads
    platform.start_threads(platform_key)

    # keep main thread alive
    platform.serve_forever()
