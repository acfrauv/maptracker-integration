# -*-* coding: utf-8 -*-*
from __future__ import print_function
from time import sleep, time
from platform_tools.platformdata import APIPlatform, get_args
import thread
import json
from random import randrange
import requests
import sys


# Platform key: must be unique
platform_key = "spot"

SERVER_URL = 'https://api.findmespot.com/spot-main-web/consumer/rest-api/2.0/public/feed/'
FEED_ID = '1vtkorULrCYqEMpYecqy37kWHbWj2JaKG'
PASSWORD = 'maptracker'
PAGE_URL = '{}{}/latest.json?feedPassword={}'.format(SERVER_URL,FEED_ID,PASSWORD)

# mails are sent every 2.5 minutes
UPDATE_PERIOD = 30

cgi_data = """
{
    "response": {
        "feedMessageResponse": {
            "count":1,
            "feed": {
                "id":"1vtkorULrCYqEMpYecqy37kWHbWj2JaKG",
                "name":"Maptracker API",
                "description":"Maptracker API",
                "status":"ACTIVE",
                "usage":0,
                "daysRange":7,
                "detailedMessageShown":true,
                "type":"SHARED_PAGE"
            },
            "totalCount":1,
            "activityCount":0,
            "messages": {
                "message": {
                    "@clientUnixTime":"0",
                    "id":1370767129,
                    "messengerId":"0-4404228",
                    "messengerName":"ACFR01",
                    "unixTime":1583372761,
                    "messageType":"EXTREME-TRACK",
                    "latitude":-33.88672,
                    "longitude":151.19928,
                    "modelId":"SPOTTRACE",
                    "showCustomMsg":"Y",
                    "dateTime":"2020-03-05T01:46:01+0000",
                    "batteryState":"GOOD",
                    "hidden":0,
                    "altitude":44
                }
            }
        }
    }
}
"""

class SPOTPlatform(APIPlatform):
    def __init__(self, **kwargs):
        # not needed for a real vehicle, this is just for fake data
        self.origin = [-33.88672, 151.19928]
        APIPlatform.__init__(self, **kwargs)

    def platformdata_update_thread(self, platform_key):
        """
        NOTE: This should be a thread that subscribes to a UDP feed or similar to receive nav updates. It should
        construct the nav data object as done here, but when new data is received.
        """
        old_id = 0
        while True:
            spot_message = self.get_spot_data()
            if spot_message is not None:
                if old_id != spot_message['response']['feedMessageResponse']['messages']['message']['id']:
                    apipost = self.build_apipost(spot_message)
                    self.update_platform_data(platform_key, apipost)
                    old_id = spot_message['response']['feedMessageResponse']['messages']['message']['id']
            # good faith API calls limit to every 2.5 min
            sleep(150)
            

    def build_apipost(self, spot_message):
        """
        Builds a valid apipost message from a spot-based dict
        
        Args:
            spot_message (dict): spot message
        
        Returns:
            dict: maptracker api message
        """
        spot_data = spot_message['response']['feedMessageResponse']['messages']['message']
        data = {
            # All alert key-value pairs are optional. For no alerts, include empty dict, ie {}
            # The keys can be anything, but keep them short for display.
            # The values need to be int 0/1, with 1=on, 0=off.
            "alert": {
                #'valid': int(spot_data['valid'])
            },

            # required: lat, lon, heading
            # optional, but required for dash display: roll, pitch, bat
            # optional, speed, uncertainty, alt, depth
            # all values must be floats
            "pose": {
                # float, decimal degrees
                "lat": spot_data['latitude'],
                # float, decimal degrees
                "lon": spot_data['longitude']
            },

            # All stat key-value pairs are optional. For no alerts, include empty dict, ie {}
            # optional, but required for dash display: bat
            # The keys can be anything, but keep them short for display.
            # The values can be anything, with the exception of bat, which must be an int
            "stat": {
                #'valid': spot_data['valid'],
                "name": spot_data['messengerName'],
                "batteryState": spot_data['batteryState'],
                "message_id": spot_data['messengerId'],
                "timestamp": spot_data['dateTime']
            }
        }
        return data


    def get_spot_data_fake(self):
        """
        Fake message generator
        
        Returns:
            dict: an spot message
        """
        spot_message = json.loads(cgi_data)
        return spot_message

    def get_spot_data(self):
        """
        Message retriever.

        GET the server for a parsed spot JSON data (CGI-BIN script)
        
        Returns:
            dict: spot message
        """
        try:
            r = requests.get(PAGE_URL)
            if r.ok:
                spot_message = r.json()
                print('spot: http request SUCCESS')
                print(spot_message)
                return spot_message
            else:
                raise requests.ConnectionError('Request fail ( HTTP {})'.format(r.status_code))
        except requests.ConnectionError as err:
            print('spot: HTTP request FAILED: {}'.format(err))
            return None
    
    def start_threads(self, platform_key):
        thread.start_new_thread(
            platform.platformdata_update_thread, (platform_key,))


if __name__ == "__main__":

    # get cli args
    args = get_args()

    # Instantiate class
    platform = SPOTPlatform(
        maptracker_url=args["url"], maptracker_port=args["port"], command_server_port=args["command_port"])

    # Start data threads
    platform.start_threads(platform_key)

    # keep main thread alive
    platform.serve_forever()
