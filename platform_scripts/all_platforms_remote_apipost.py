
from platform_tools.platformdata import post_data
from time import sleep
import requests

# "targets/acfr-ship-falkor.json",
# "targets/acfr-nga.json",
# "targets/acfr-nga.usbl.json",
# "targets/umich-iver.json",
# "targets/umich-iver.usbl.json",
# "targets/umich-iver.iridium.json",
# "targets/whoi-slocum.json",
# "targets/acfr-nav-orangeBox.json",
# "targets/acfr-sirius.usbl.json",
# "targets/acfr-sirius.json",
# "targets/uri-lfloat.json",
# "targets/uri-lfloat.usbl.json",
# "targets/acfr-holt.json",
# "targets/acfr-holt.usbl.json",

remote_url = "http://144.6.225.82"
remote_port = 80
local_url = "http://localhost"
local_port = 8080

settings = {
    "remote_url": remote_url,
    "remote_port": remote_port,
    "post_delay": 10,                        # seconds
    "data": [
        # platforms
        # {"type": "platform", "key": "test", "url": "http://localhost:8080/data/platformdata.api.Platform/test"},
        {"type": "platform", "key": "SIRIUS.AUVSTAT", "url": "http://localhost:8080/data/platformdata.api.Platform/SIRIUS.AUVSTAT"},
        {"type": "platform", "key": "FALKOR.USBL_FIX.SIRIUS", "url": "http://localhost:8080/data/platformdata.api.Platform/FALKOR.USBL_FIX.SIRIUS"},
        {"type": "platform", "key": "HOLT.AUVSTAT", "url": "http://localhost:8080/data/platformdata.api.Platform/HOLT.AUVSTAT"},
        {"type": "platform", "key": "FALKOR.USBL_FIX.HOLT", "url": "http://localhost:8080/data/platformdata.api.Platform/FALKOR.USBL_FIX.HOLT"},
        {"type": "platform", "key": "NGA.AUVSTAT", "url": "http://localhost:8080/data/platformdata.api.Platform/NGA.AUVSTAT"},
        {"type": "platform", "key": "FALKOR.USBL_FIX.NGA","url": "http://localhost:8080/data/platformdata.api.Platform/FALKOR.USBL_FIX.NGA"},
        {"type": "platform", "key": "FALKOR.USBL_FIX.PFLOAT", "url": "http://localhost:8080/data/platformdata.api.Platform/FALKOR.USBL_FIX.PFLOAT"},
        {"type": "platform", "key": "PFLOAT.AUVSTAT", "url": "http://localhost:8080/data/platformdata.api.Platform/PFLOAT.AUVSTAT"},
        {"type": "platform", "key": "whoi.slocum", "url": "http://localhost:8080/data/platformdata.api.Platform/whoi.slocum"},
        {"type": "platform", "key": "DROPIVER.AUVSTAT", "url": "http://localhost:8080/data/platformdata.api.Platform/DROPIVER.AUVSTAT"},
        {"type": "platform", "key": "FALKOR.USBL_FIX.DROPIVER", "url": "http://localhost:8080/data/platformdata.api.Platform/FALKOR.USBL_FIX.DROPIVER"},
        # {"type": "platform", "key": "DROPIVER.IRIDIUM", "url": "http://localhost:8080/data/platformdata.api.Platform/DROPIVER.IRIDIUM"},

        # ship
        {"type": "platform", "key": "ORANGEBOX.SHIP_STATUS", "url": "http://localhost:8080/data/platformdata.api.Platform/ORANGEBOX.SHIP_STATUS"},
        {"type": "platform", "key": "FALKOR.SHIP_STATUS", "url": "http://localhost:8080/data/platformdata.api.Platform/FALKOR.SHIP_STATUS"},
        {"type": "platform", "key": "ATREU.AIS", "url": "http://localhost:8080/data/platformdata.api.Platform/ATREU.AIS"},

        # missions
        {"type": "mission", "key": "umich.iver", "url": "http://localhost:8080/get_mission/platformdata.api.Platform/umich.iver"},
        {"type": "mission", "key": "acfr.sirius", "url": "http://localhost:8080/get_mission/platformdata.api.Platform/acfr.sirius"},
        {"type": "mission", "key": "whoi.slocum", "url": "http://localhost:8080/get_mission/platformdata.api.Platform/whoi.slocum"},
        {"type": "mission", "key": "AIS_LAYER", "url": "http://localhost:8080/get_mission/platformdata.api.Platform/AIS_LAYER"}
    ],
    "remote": [
        {"type": "platform", "key": "ANDROID.TERMUX", "url": "{}/data/platformdata.api.Platform/ANDROID.TERMUX".format(remote_url)}
    ]
}





target_data = {}
remote_data = {}

while True:
    # post to remote
    for t in settings['data']:
        try:
            r = requests.get(t["url"])
            data = r.json()
            cache_key = t["type"]+"-"+t["key"]
            if cache_key in target_data:
                do_post = data["msgts"] > target_data[cache_key]
            else:
                do_post = True
            if do_post:
                post_data(t["key"], data, data_type=t["type"], maptracker_url=settings["remote_url"], maptracker_port=settings["remote_port"])
                target_data[cache_key] = data["msgts"]
        except Exception as e:
            print "*** ERROR posting to remote: {} ({})! - {}".format(t["key"], t["type"], e)

    # get remote data
    for t in settings['remote']:
        try:
            r = requests.get(t["url"])
            data = r.json()
            cache_key = t["type"] + "-" + t["key"]
            if cache_key in remote_data:
                do_post = data["msgts"] > remote_data[cache_key]
            else:
                do_post = True
            if do_post:
                post_data(t["key"], data, data_type=t["type"], maptracker_url=local_url, maptracker_port=local_port)
                remote_data[cache_key] = data["msgts"]
        except Exception as e:
            print "*** ERROR getting remote data: {} ({})! - {}".format(t["key"], t["type"], e)

    sleep(settings["post_delay"])