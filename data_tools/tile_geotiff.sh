#!/bin/bash

# SETUP GDAL
# ./install_gdal.sh


FILEIN=$1

fname=$(basename $FILEIN)
extension="${fname##*.}"
filename="${fname%.*}"
dname=$(dirname $FILEIN)

GDAL2TILES="gdal2tiles.py"                           # default that comes with gdal
GDAL2TILES_MULTI="python gdal2tiles-multiprocess.py"    # multiprocess version


#FILEOUT="${dname}/${filename}-converted.${extension}"
## Correct map projection and make background transparent
#gdalwarp ${FILEIN} ${FILEOUT} -t_srs "+proj=longlat +ellps=WGS84" -wo NUM_THREADS=ALL_CPUS -dstnodata 255
#tiff_files=`ls image_r*_c*_rs*_cs*.tif`
#gdalbuildvrt -a_srs mosaic-corrected.vrt $tiff_files
#gdalwarp ${FILEIN} ${FILEOUT} -t_srs 'epsg:900913'  -wo NUM_THREADS=ALL_CPUS -dstnodata 255

# create tiles
TILEDIR="${dname}/${filename}-tiles"
#mkdir $TILEDIR
$GDAL2TILES_MULTI -z 14-18 -w none ${FILEIN} ${TILEDIR}
$GDAL2TILES -z 1-13 -w none ${FILEIN} ${TILEDIR}
